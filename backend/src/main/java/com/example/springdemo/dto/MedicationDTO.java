package com.example.springdemo.dto;

import com.example.springdemo.entities.Medication;

public class MedicationDTO {
    private Integer id;
    private String name;
    private String side_effects;
    private String dosage;

    public MedicationDTO(Integer id, String name, String side_effects, String dosage) {
        this.id = id;
        this.name = name;
        this.side_effects = side_effects;
        this.dosage = dosage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSide_effects() {
        return side_effects;
    }

    public void setSide_effects(String side_effects) {
        this.side_effects = side_effects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }
}
