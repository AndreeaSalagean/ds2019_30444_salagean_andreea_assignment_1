package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.entities.Medication;

public class MedicationBuilder {
    public static MedicationDTO generateDTOFromEntity(Medication med){
        return new MedicationDTO(
                med.getId(),
                med.getName(),
                med.getSide_effects(),
                med.getDosage());
    }

    public static Medication generateEntityFromDTO(MedicationDTO medDTO){
        return new Medication(
                medDTO.getId(),
                medDTO.getName(),
                medDTO.getSide_effects(),
                medDTO.getDosage());
    }
}
