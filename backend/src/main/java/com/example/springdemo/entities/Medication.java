package com.example.springdemo.entities;

import javax.persistence.*;

import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication")
public class Medication {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "side_effects")
    private String side_effects;

    @Column(name = "dosage")
    private String dosage;

    @ManyToMany(mappedBy = "medications")
    List<MedicationPlan> medicationPlans ;

    @OneToMany(mappedBy = "medication")
    private List<Intake_intervals> intake_intervals;


    public Medication(){

    }

    public Medication(Integer id, String name, String side_effects, String dosage){
        this.id = id;
        this.name = name;
        this. side_effects = side_effects;
        this.dosage = dosage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSide_effects() {
        return side_effects;
    }

    public void setSide_effects(String side_effects) {
        this.side_effects = side_effects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }
}
