package com.example.springdemo.entities;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "intake_interval")
public class Intake_intervals {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "start")
    private String start;

    @Column(name = "end")
    private String end;

    @ManyToOne
    @JoinColumn(name = "med_plan_id", nullable = false)
    private MedicationPlan medication_plan;

    @ManyToOne
    @JoinColumn(name = "med_id", nullable = false)
    private Medication medication;
}
