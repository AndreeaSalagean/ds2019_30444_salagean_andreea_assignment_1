package com.example.springdemo.services;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.dto.PersonDTO;
import com.example.springdemo.dto.builders.DoctorBuilder;
import com.example.springdemo.entities.Doctor;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.DoctorRepository;
import com.example.springdemo.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.print.Doc;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.springdemo.dto.builders.DoctorBuilder.generateEntityFromDTO;

@Service
public class DoctorService {

    private final DoctorRepository doctorRepository;
    private final PersonRepository personRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository, PersonRepository personRepository){this.doctorRepository = doctorRepository;
    this.personRepository = personRepository;}

    public DoctorDTO findUserById(Integer id){
        Optional<Doctor> person  = doctorRepository.findById(id);

        if (!person.isPresent()) {
            throw new ResourceNotFoundException("Doctor", "user id", id);
        }
        return DoctorBuilder.generateDTOFromEntity(person.get());
    }

    public List<DoctorDTO> getAll(){
        List<Doctor> doctors = doctorRepository.findAll();
        return doctors.stream()
                .map(DoctorBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(DoctorDTO doctorDTO) {

        //PersonFieldValidator.validateInsertOrUpdate(personDTO);

        return doctorRepository
                .save(generateEntityFromDTO(doctorDTO))
                .getId();
    }

    public Integer update(DoctorDTO doctorDTO) {

        Optional<Doctor> person = doctorRepository.findById(doctorDTO.getId());

        if(!person.isPresent()){
            throw new ResourceNotFoundException("Doctor", "id", doctorDTO.getId().toString());
        }
        //PersonFieldValidator.validateInsertOrUpdate(personDTO);

        return doctorRepository.save(generateEntityFromDTO(doctorDTO)).getId();
    }

    public void delete(DoctorDTO doctorDTO){
        this.personRepository.deleteById(generateEntityFromDTO(doctorDTO).getId());
      //  this.doctorRepository.deleteById(doctorDTO.getId());
    }

}
