package com.example.springdemo.services;

import com.example.springdemo.dto.PPerson;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.PersonDTO;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.dto.builders.PersonBuilder;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.Person;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.PatientRepository;
import com.example.springdemo.repositories.PersonRepository;
import com.example.springdemo.validators.PersonFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.springdemo.dto.builders.PatientBuilder.generateEntityFromDTO;

@Service
public class PatientService {
    private final PatientRepository patientRepository;
    private final PersonRepository personRepository;


    @Autowired
    public PatientService(PatientRepository patientRepository, PersonRepository personRepository){this.patientRepository = patientRepository;
            this.personRepository = personRepository;
    }

    public PatientDTO findUserById(Integer id){
        Optional<Patient> person  = patientRepository.findById(id);

        if (!person.isPresent()) {
            throw new ResourceNotFoundException("Patient", "user id", id);
        }
        return PatientBuilder.generateDTOFromEntity(person.get());
    }

    public List<PatientDTO> getAll(){
        List<Patient> patients = patientRepository.findAll();
        return patients.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(PPerson person) {
       // System.out.println("TEROGGGG" +patientDTO.getId() );
        Patient patient = new Patient(null, person.getName(), person.getBirthDate(), person.getGender(), person.getAddress(), person.getMedicalRecord());
        Person personToAdd = new Person(null, person.getEmail(), person.getPassword(), "patient");
        patient.setPerson(personToAdd);

        return patientRepository
                .save(patient)
                .getId();
    }

    public Integer update(PatientDTO patientDTO) {

        Optional<Patient> person = patientRepository.findById(patientDTO.getId());

        if(!person.isPresent()){
            throw new ResourceNotFoundException("Patient", "id", patientDTO.getId().toString());
        }
        //PersonFieldValidator.validateInsertOrUpdate(personDTO);

        return patientRepository.save(generateEntityFromDTO(patientDTO)).getId();
    }

    public void delete(Integer id){
       this.personRepository.deleteById(id);
    }
}
