package com.example.springdemo.controller;

import com.example.springdemo.dto.PPerson;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.PersonDTO;
import com.example.springdemo.dto.PersonViewDTO;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/patient")
public class PatientController  {
    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService){this.patientService = patientService;}

    @GetMapping(value = "/{id}")
    public PatientDTO findById(@PathVariable("id") Integer id){
        return patientService.findUserById(id);
    }

    @GetMapping()
    public List<PatientDTO> findAll(){
        return patientService.getAll();
    }

    @PostMapping()
    public Integer insertPatient(@RequestBody PPerson person) {
        return patientService.insert(person);
    }

    @PutMapping(value = "/{id}")
    public Integer updatePatient(@RequestBody PatientDTO patientDTO) {
        return patientService.update(patientDTO);
    }

    @DeleteMapping(value = "/{id}")
    public void deletePatient(@PathVariable("id") Integer id){
        patientService.delete(id);
    }


}
