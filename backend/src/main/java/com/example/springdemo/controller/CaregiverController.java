package com.example.springdemo.controller;


import com.example.springdemo.dto.*;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping(value = "/{id}")
    public CaregiverDTO findById(@PathVariable("id") Integer id){
        return caregiverService.findUserById(id);
    }

    @GetMapping()
    public List<CaregiverDTO> findAll(){
        return caregiverService.getAll();
    }

    @GetMapping(value = "/{id}/patients-list")
    public List<PatientDTO> findPatientList(@PathVariable("id") Integer id){
        System.out.println(id);
        return caregiverService.getPatientList(id);
    }
    @GetMapping(value = "/{id}/patients-list-available")
    public List<PatientDTO> findAvailableList(@PathVariable("id") Integer id){
        return caregiverService.getAvailablePatients(id);
    }

    @PostMapping(value = "/{id}/add-patient")
    public void insertPatientDTO(@RequestBody PatientDTO patient, @PathVariable("id") Integer id){ caregiverService.insertPatient(patient,id);
    System.out.println(id);
    System.out.println(patient);}

    @PostMapping()
    public Integer insertCaregiverDTO(@RequestBody CPerson person){
        return caregiverService.insert(person);
    }

    @PutMapping(value = "/{id}")
    public Integer updateCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        return caregiverService.update(caregiverDTO);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Integer id){
        caregiverService.delete(id);
    }

    @DeleteMapping(value = "/{id}/delete-patient/{idP}")
    public void deletePatient(@PathVariable("id") Integer id, @PathVariable("idP") Integer idP){
        caregiverService.deletePatient(id, idP);
    }

}
