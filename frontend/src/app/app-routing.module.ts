import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListPatientComponent} from './components/patient/list-patient/list-patient.component';
import {EditPatientComponent} from './components/patient/edit-patient/edit-patient.component';
import {CreatePatientComponent} from './components/patient/create-patient/create-patient.component';
import {ListCaregiverComponent} from './components/caregiver/list-caregiver/list-caregiver.component';
import {DetailsPatientComponent} from './components/patient/details-patient/details-patient.component';
import {DetailsCaregiverComponent} from './components/caregiver/details-caregiver/details-caregiver.component';
import {EditCaregiverComponent} from './components/caregiver/edit-caregiver/edit-caregiver.component';
import {CreateCaregiverComponent} from './components/caregiver/create-caregiver/create-caregiver.component';
import {ListMedicationComponent} from './components/medication/list-medication/list-medication.component';
import {EditMedicationComponent} from './components/medication/edit-medication/edit-medication.component';
import {CreateMedicationComponent} from './components/medication/create-medication/create-medication.component';
import {DetailsMedicationComponent} from './components/medication/details-medication/details-medication.component';
import {WelcomeComponent} from './components/welcome/welcome.component';
import {LoginComponent} from './components/login/login.component';
import {AuthGuard} from './helpers/authGuard';
import {Role} from './models/role';
import {PopupListComponent} from './components/popup-list/popup-list.component';
import {CaregiverPatientsComponent} from './components/caregiver/caregiver-patients/caregiver-patients.component';



const routes: Routes = [{ path: '',
  component: WelcomeComponent,
  canActivate: [AuthGuard]},
  {path: 'patient', component: ListPatientComponent, canActivate: [AuthGuard],
    data: { roles: [Role.Doctor]  }},
  { path: 'edit-patient', component: EditPatientComponent , canActivate: [AuthGuard],
    data: { roles: [Role.Doctor] }},
  { path: 'add-patient', component: CreatePatientComponent , canActivate: [AuthGuard],
    data: { roles: [Role.Doctor] }},
  { path: 'caregiver', component: ListCaregiverComponent, canActivate: [AuthGuard],
    data: { roles: [Role.Doctor] }},
  { path: 'details/:id', component: DetailsPatientComponent, canActivate: [AuthGuard],
    data: { roles: [Role.Doctor] }},
  { path: 'details-caregiver/:id', component: DetailsCaregiverComponent, canActivate: [AuthGuard],
    data: { roles: [Role.Doctor] }},
  { path: 'edit-caregiver', component: EditCaregiverComponent, canActivate: [AuthGuard],
    data: { roles: [Role.Doctor] } },
  { path: 'add-caregiver', component: CreateCaregiverComponent, canActivate: [AuthGuard],
    data: { roles: [Role.Doctor] } },
  { path: 'medication', component: ListMedicationComponent, canActivate: [AuthGuard],
    data: { roles: [Role.Doctor] }},
  { path: 'edit-medication', component: EditMedicationComponent, canActivate: [AuthGuard],
    data: { roles: [Role.Doctor] } },
  { path: 'add-medication', component: CreateMedicationComponent, canActivate: [AuthGuard],
    data: { roles: [Role.Doctor] } },
  { path: 'details-medication/:id', component: DetailsMedicationComponent, canActivate: [AuthGuard],
    data: { roles: [Role.Doctor] }},
  { path: 'list', component: WelcomeComponent},
  { path: 'login', component: LoginComponent},
  { path: 'popup/:id', component: PopupListComponent},
  { path: 'patientsList', component: CaregiverPatientsComponent, canActivate: [AuthGuard],
    data: { roles: [Role.Caregiver]  }}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
