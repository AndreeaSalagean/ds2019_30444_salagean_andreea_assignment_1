import {Role} from './role';

export class Person {
  id: number;
  email: string;
  password: string;
  role: Role;
  token?: string;

  constructor(id: number, email: string, password: string) {
    this.id = id;
    this.email = email;
    this.password = password;
}
}
