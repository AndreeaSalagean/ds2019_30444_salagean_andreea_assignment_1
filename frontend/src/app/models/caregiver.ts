export class Caregiver {
  id: number;
  name: string;
  birthDate: string;
  gender: string;
  address: string;
  constructor(id: number, name: string, birthDate: string, gender: string, address: string) {
    this.id = id;
    this.name = name;
    this.birthDate = birthDate;
    this.gender = gender;
    this.address = address;
  }
}
