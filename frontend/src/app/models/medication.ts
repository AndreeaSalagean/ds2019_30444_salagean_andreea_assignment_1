export class Medication {
  id: number;
  name: string;
  side_effects: string;
  dosage: string;
}
