export class Patient {
  id: number;
  name: string;
  birthDate: string;
  gender: string;
  address: string;
  medicalRecord: string;
  constructor(id: number, name: string, birthDate: string, gender: string, address: string, medicalRecord: string) {
    this.id = id;
    this.name = name;
    this.birthDate = birthDate;
    this.gender = gender;
    this.address = address;
    this.medicalRecord = medicalRecord;
  }
}
