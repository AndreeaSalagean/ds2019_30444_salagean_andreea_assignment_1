export enum Role {
  Patient = 'patient',
  Doctor = 'doctor',
  Caregiver = 'caregiver'
}
