export class PersonPatient {
  email: string;
  password: string;
  name: string;
  birthDate: string;
  gender: string;
  address: string;
  medicalRecord: string;
}
