export class PersonCaregiver {
  email: string;
  password: string;
  name: string;
  birthDate: string;
  gender: string;
  address: string;
}
