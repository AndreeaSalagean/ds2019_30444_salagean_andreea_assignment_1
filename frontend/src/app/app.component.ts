import { Component } from '@angular/core';
import {Router} from '@angular/router';
import {Person} from './models/person';
import {AuthenticationService} from './services/authentication.service';
import {Role} from './models/role';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-project';
  currentUser: Person;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  get isDoctor() {
    return this.currentUser && this.currentUser.role === Role.Doctor;
  }
  get isPatient() {
    return this.currentUser && this.currentUser.role === Role.Patient;
  }
  get isCaregiver() {
    return this.currentUser && this.currentUser.role === Role.Caregiver;
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
