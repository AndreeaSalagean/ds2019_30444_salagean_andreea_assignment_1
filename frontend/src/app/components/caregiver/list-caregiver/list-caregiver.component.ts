import { Component, OnInit } from '@angular/core';
import {Patient} from '../../../models/patient';
import {PatientService} from '../../../services/patient.service';
import {Router} from '@angular/router';
import {Caregiver} from '../../../models/caregiver';
import {CaregiverService} from '../../../services/caregiver.service';

@Component({
  selector: 'app-list-caregiver',
  templateUrl: './list-caregiver.component.html',
  styleUrls: ['./list-caregiver.component.css']
})
export class ListCaregiverComponent implements OnInit {

  public caregivers: Caregiver[] = [];
  patients: Patient[];
  constructor(private caregiverSErvice: CaregiverService,
              private router: Router) { }

  ngOnInit() {
    this.caregiverSErvice.getCaregivers() .subscribe( data => {
      this.caregivers = data;
    });
    console.log(this.caregivers);
      }
  deleteCaregiver(caregiver: Caregiver): void {
    this.caregiverSErvice.deleteCaregiver(caregiver.id)
      .subscribe( data => {
        this.caregivers = this.caregivers.filter(u => u !== caregiver);
      });
  }

  editCaregiver(caregiver: Caregiver): void {
    window.localStorage.removeItem('editCaregiverId');
    window.localStorage.setItem('editCaregiverId', caregiver.id.toString());
    this.router.navigate(['edit-caregiver']);
  }


  caregiverDetail(id: number) {
    this.router.navigate(['details-caregiver', id]);
  }
}
