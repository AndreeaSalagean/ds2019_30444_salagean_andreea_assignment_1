import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCaregiverComponent } from './list-caregiver.component';

describe('ListCaregiverComponent', () => {
  let component: ListCaregiverComponent;
  let fixture: ComponentFixture<ListCaregiverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCaregiverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCaregiverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
