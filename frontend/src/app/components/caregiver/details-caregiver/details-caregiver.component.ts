import {Component, OnInit, ViewChild} from '@angular/core';
import {Patient} from '../../../models/patient';
import {ActivatedRoute, Router} from '@angular/router';
import {PatientService} from '../../../services/patient.service';
import {Caregiver} from '../../../models/caregiver';
import {CaregiverService} from '../../../services/caregiver.service';
import {MatBottomSheet} from '@angular/material';
import {PopupListComponent} from '../../popup-list/popup-list.component';


@Component({
  selector: 'app-details-caregiver',
  templateUrl: './details-caregiver.component.html',
  styleUrls: ['./details-caregiver.component.css']
})
export class DetailsCaregiverComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router,
              private caregiverService: CaregiverService) { }

  id: number;
  caregiver: Caregiver;
  patients: Patient[];


  ngOnInit() {
    this.id = this.route.snapshot.params.id;

    this.caregiverService.getUserById(this.id)
      .subscribe(data => {
        console.log(data)
        this.caregiver = data;
      }, error => console.log(error));
    this.caregiverService.getPatients(this.id).subscribe(data => this.patients = data);
  }

  list() {
    this.router.navigate(['caregiver']);
  }


  deletePatient(patient: Patient): void {
    this.caregiverService.deletePatient(this.id, patient.id).subscribe(data =>{
      this.patients = this.patients.filter(u => u !== patient);
    });
  }

  addPatient(id: number) {
    this.router.navigate(['popup', id]);
  }
}
