import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsCaregiverComponent } from './details-caregiver.component';

describe('DetailsCaregiverComponent', () => {
  let component: DetailsCaregiverComponent;
  let fixture: ComponentFixture<DetailsCaregiverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsCaregiverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsCaregiverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
