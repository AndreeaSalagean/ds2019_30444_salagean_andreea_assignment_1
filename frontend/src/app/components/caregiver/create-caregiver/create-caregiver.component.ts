import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {PatientService} from '../../../services/patient.service';
import {Person} from '../../../models/person';
import {Patient} from '../../../models/patient';
import {CaregiverService} from '../../../services/caregiver.service';
import {Caregiver} from '../../../models/caregiver';

@Component({
  selector: 'app-create-caregiver',
  templateUrl: './create-caregiver.component.html',
  styleUrls: ['./create-caregiver.component.css']
})
export class CreateCaregiverComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private router: Router, private caregiverService: CaregiverService) { }

  addForm: FormGroup;
  caregiver: Caregiver;
  id: number;
  ngOnInit() {
    this.addForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      name: ['', Validators.required],
      birthDate: ['', Validators.required],
      gender: ['', Validators.required],
      address: ['', Validators.required]
    });

  }

  onSubmit() {
    this.caregiverService.createCaregiver(this.addForm.value)
      .subscribe( data => {
        this.router.navigate(['caregiver']);
      });

  }

}
