import { Component, OnInit } from '@angular/core';
import {Patient} from '../../../models/patient';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {PatientService} from '../../../services/patient.service';
import {first} from 'rxjs/operators';
import {Caregiver} from '../../../models/caregiver';
import {CaregiverService} from '../../../services/caregiver.service';

@Component({
  selector: 'app-edit-caregiver',
  templateUrl: './edit-caregiver.component.html',
  styleUrls: ['./edit-caregiver.component.css']
})
export class EditCaregiverComponent implements OnInit {

  id: number;
  caregiver: Caregiver;
  editForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private router: Router, private caregiverService: CaregiverService) { }

  ngOnInit() {
    const caregiverId = window.localStorage.getItem('editCaregiverId');
    if (!caregiverId) {
      alert('Invalid action.');
      this.router.navigate(['list-caregiver']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      birthDate: ['', Validators.required],
      gender: ['', Validators.required],
      address: ['', Validators.required],
    });
    this.caregiverService.getUserById(+caregiverId)
      .subscribe( data => {
        this.caregiver = new Caregiver(data.id, data.name, data.birth_date, data.gender, data.address);
        this.editForm.setValue({id : this.caregiver.id, name : this.caregiver.name,
          birthDate : this.caregiver.birthDate, gender : this.caregiver.gender, address: this.caregiver.address});
      });
  }
  onSubmit() {
    this.caregiverService.updateCaregiver(this.editForm.value)
      .pipe(first())
      .subscribe(
        data => {
          alert('User updated successfully.');
          this.router.navigate(['caregiver']);
        },
        error => {
          alert(error);
        });
  }

}
