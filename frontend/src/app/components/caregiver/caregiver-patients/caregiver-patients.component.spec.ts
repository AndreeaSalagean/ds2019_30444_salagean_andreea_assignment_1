import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaregiverPatientsComponent } from './caregiver-patients.component';

describe('CaregiverPatientsComponent', () => {
  let component: CaregiverPatientsComponent;
  let fixture: ComponentFixture<CaregiverPatientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaregiverPatientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaregiverPatientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
