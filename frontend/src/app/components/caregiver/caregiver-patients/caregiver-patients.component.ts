import { Component, OnInit } from '@angular/core';
import {Patient} from '../../../models/patient';
import {ActivatedRoute, Router} from '@angular/router';
import {CaregiverService} from '../../../services/caregiver.service';
import {AuthenticationService} from '../../../services/authentication.service';
import {Person} from '../../../models/person';

@Component({
  selector: 'app-caregiver-patients',
  templateUrl: './caregiver-patients.component.html',
  styleUrls: ['./caregiver-patients.component.css']
})
export class CaregiverPatientsComponent implements OnInit {
  patients: Patient[];
  id: number;
  currentUser: Person;
  constructor(private route: ActivatedRoute, private router: Router,
              private caregiverService: CaregiverService, private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit() {
    this.id = this.currentUser.id;
    this.caregiverService.getPatients(this.id).subscribe(data => {
      this.patients = data;
    });
  }

}
