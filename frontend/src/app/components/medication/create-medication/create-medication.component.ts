import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {CaregiverService} from '../../../services/caregiver.service';
import {Caregiver} from '../../../models/caregiver';
import {MedicationService} from '../../../services/medication.service';
import {Medication} from '../../../models/medication';

@Component({
  selector: 'app-create-medication',
  templateUrl: './create-medication.component.html',
  styleUrls: ['./create-medication.component.css']
})
export class CreateMedicationComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private router: Router, private medicationService: MedicationService) { }

  addForm: FormGroup;
  medication: Medication;
  id: number;
  ngOnInit() {
    this.addForm = this.formBuilder.group({
      name: ['', Validators.required],
      side_effects: ['', Validators.required],
      dosage: ['', Validators.required],
    });

  }

  onSubmit() {
    this.medicationService.createMedication(this.addForm.value)
      .subscribe( data => {
        this.router.navigate(['medication']);
      });

  }

}
