import { Component, OnInit } from '@angular/core';
import {Caregiver} from '../../../models/caregiver';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {CaregiverService} from '../../../services/caregiver.service';
import {first} from 'rxjs/operators';
import {Medication} from '../../../models/medication';
import {MedicationService} from '../../../services/medication.service';

@Component({
  selector: 'app-edit-medication',
  templateUrl: './edit-medication.component.html',
  styleUrls: ['./edit-medication.component.css']
})
export class EditMedicationComponent implements OnInit {

  id: number;
  medication: Medication;
  editForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private router: Router, private medicationService: MedicationService) { }

  ngOnInit() {
    const medId = window.localStorage.getItem('editMedId');
    if (!medId) {
      alert('Invalid action.');
      this.router.navigate(['list-medication']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      side_effects: ['', Validators.required],
      dosage: ['', Validators.required],
    });
    this.medicationService.getMedById(+medId)
      .subscribe( data => {
        this.editForm.setValue({id : data.id, name : data.name,
          side_effects : data.side_effects, dosage : data.dosage});
      });
  }
  onSubmit() {
    this.medicationService.updateMedication(this.editForm.value)
      .pipe(first())
      .subscribe(
        data => {
          alert('User updated successfully.');
          this.router.navigate(['medication']);
        },
        error => {
          alert(error);
        });
  }

}
