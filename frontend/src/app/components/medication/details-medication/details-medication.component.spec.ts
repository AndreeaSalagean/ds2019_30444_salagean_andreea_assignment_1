import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsMedicationComponent } from './details-medication.component';

describe('DetailsMedicationComponent', () => {
  let component: DetailsMedicationComponent;
  let fixture: ComponentFixture<DetailsMedicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsMedicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsMedicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
