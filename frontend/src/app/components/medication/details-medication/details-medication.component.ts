import { Component, OnInit } from '@angular/core';
import {Caregiver} from '../../../models/caregiver';
import {ActivatedRoute, Router} from '@angular/router';
import {CaregiverService} from '../../../services/caregiver.service';
import {Medication} from '../../../models/medication';
import {MedicationService} from '../../../services/medication.service';

@Component({
  selector: 'app-details-medication',
  templateUrl: './details-medication.component.html',
  styleUrls: ['./details-medication.component.css']
})
export class DetailsMedicationComponent implements OnInit {

  id: number;
  medication: Medication;

  constructor(private route: ActivatedRoute, private router: Router,
              private medicationService: MedicationService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;

    this.medicationService.getMedById(this.id)
      .subscribe(data => {
        console.log(data)
        this.medication = data;
      }, error => console.log(error));
  }

  list() {
    this.router.navigate(['medication']);
  }

}
