import { Component, OnInit } from '@angular/core';
import {Caregiver} from '../../../models/caregiver';
import {CaregiverService} from '../../../services/caregiver.service';
import {Router} from '@angular/router';
import {Medication} from '../../../models/medication';
import {MedicationService} from '../../../services/medication.service';

@Component({
  selector: 'app-list-medication',
  templateUrl: './list-medication.component.html',
  styleUrls: ['./list-medication.component.css']
})
export class ListMedicationComponent implements OnInit {

  public medications: Medication[] = [];
  constructor(private medicationService: MedicationService,
              private router: Router) { }

  ngOnInit() {
    this.medicationService.getMedication() .subscribe( data => {
      this.medications = data;
    });
  }
  deleteMedication(medication: Medication): void {
    this.medicationService.deleteMedication(medication.id)
      .subscribe( data => {
        this.medications = this.medications.filter(u => u !== medication);
      });
  }

  editMedication(med: Medication): void {
    window.localStorage.removeItem('editMedId');
    window.localStorage.setItem('editMedId', med.id.toString());
    this.router.navigate(['edit-medication']);
  }


  medicationDetail(id: number) {
    this.router.navigate(['details-medication', id]);
  }

}
