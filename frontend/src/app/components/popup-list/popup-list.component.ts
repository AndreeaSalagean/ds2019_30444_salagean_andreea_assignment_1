import { Component, OnInit } from '@angular/core';
import {PatientService} from '../../services/patient.service';
import {Patient} from '../../models/patient';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {CaregiverService} from '../../services/caregiver.service';

@Component({
  selector: 'app-popup-list',
  templateUrl: './popup-list.component.html',
  styleUrls: ['./popup-list.component.css']
})
export class PopupListComponent implements OnInit {

  patients: Patient[];
  size: number;
  constructor(private route: ActivatedRoute, private router: Router,
              private caregiverService: CaregiverService) {}
  id: number;

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.caregiverService.getAvailablePat(this.id).subscribe(data => {
      this.patients = data;
    });
  }

  addPatient(patient: Patient) {
    this.caregiverService.addPatient(patient, this.id)
      .subscribe( data => {
        this.router.navigate(['details-caregiver', this.id]);
      });
  }
}
