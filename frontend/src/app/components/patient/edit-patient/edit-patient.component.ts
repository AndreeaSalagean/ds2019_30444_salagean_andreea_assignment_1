import { Component, OnInit } from '@angular/core';
import {Patient} from '../../../models/patient';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {PatientService} from '../../../services/patient.service';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-edit-patient',
  templateUrl: './edit-patient.component.html',
  styleUrls: ['./edit-patient.component.css']
})
export class EditPatientComponent implements OnInit {

  id: number;
  patient: Patient;
  editForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private router: Router, private patientService: PatientService) { }

  ngOnInit() {
    const patientId = window.localStorage.getItem('editPatientId');
    if (!patientId) {
      alert('Invalid action.');
      this.router.navigate(['list-patient']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      birthDate: ['', Validators.required],
      gender: ['', Validators.required],
      address: ['', Validators.required],
      medicalRecord: ['', Validators.required]
    });
    this.patientService.getUserById(+patientId)
      .subscribe( data => {
        this.patient = new Patient(data.id, data.name, data.birth_date, data.gender, data.address, data.medicalRecord);
        this.editForm.setValue({id : this.patient.id, name : this.patient.name,
          birthDate : this.patient.birthDate, gender : this.patient.gender, address: this.patient.address,
          medicalRecord: this.patient.medicalRecord});
      });
  }
  onSubmit() {
    this.patientService.updatePatient(this.editForm.value)
      .pipe(first())
      .subscribe(
        data => {
            alert('User updated successfully.');
            this.router.navigate(['patient']);
        },
        error => {
          alert(error);
        });
  }
}
