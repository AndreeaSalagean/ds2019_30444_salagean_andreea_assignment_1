import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {PatientService} from '../../../services/patient.service';
import {Person} from '../../../models/person';
import {Patient} from '../../../models/patient';
import {error} from 'util';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-create-patient',
  templateUrl: './create-patient.component.html',
  styleUrls: ['./create-patient.component.css']
})
export class CreatePatientComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private router: Router, private patientService: PatientService) { }

  addForm: FormGroup;
  person: Person;
  patient: Patient;
  id: number;
  ngOnInit() {
    this.addForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      name: ['', Validators.required],
      birthDate: ['', Validators.required],
      gender: ['', Validators.required],
      address: ['', Validators.required],
    medicalRecord: ['', Validators.required]
    });

  }

  onSubmit() {
    this.patientService.createPatient(this.addForm.value)
      .subscribe( data => {
        this.router.navigate(['patient']);
      });

   }

}
