import { Component, OnInit } from '@angular/core';
import {Patient} from '../../../models/patient';
import {ActivatedRoute, Router} from '@angular/router';
import {PatientService} from '../../../services/patient.service';

@Component({
  selector: 'app-details-patient',
  templateUrl: './details-patient.component.html',
  styleUrls: ['./details-patient.component.css']
})
export class DetailsPatientComponent implements OnInit {

  id: number;
  patient: Patient;

  constructor(private route: ActivatedRoute, private router: Router,
              private patientSrvice: PatientService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;

    this.patientSrvice.getUserById(this.id)
      .subscribe(data => {
        console.log(data)
        this.patient = data;
      }, error => console.log(error));
  }

  list() {
    this.router.navigate(['patient']);
  }

}
