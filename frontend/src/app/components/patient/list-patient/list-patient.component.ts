import { Component, OnInit } from '@angular/core';
import {Patient} from '../../../models/patient';
import {Router} from '@angular/router';
import {PatientService} from '../../../services/patient.service';

@Component({
  selector: 'app-list-patient',
  templateUrl: './list-patient.component.html',
  styleUrls: ['./list-patient.component.css']
})
export class ListPatientComponent implements OnInit {

  public patients: Patient[] = [];
  constructor(private patientService: PatientService,
              private router: Router) { }

  ngOnInit() {
    this.patientService.getPatients()
      .subscribe( (data: Patient[]) => {
          data.forEach(element => {
          // @ts-ignore
          this.patients.push(new Patient(element.id, element.name, element.birth_date, element.gender, element.address));
        })

          console.log(this.patients);

      });
  }

  deletePatient(patient: Patient): void {
    this.patientService.deleteUser(patient.id)
      .subscribe( data => {
        this.patients = this.patients.filter(u => u !== patient);
      });
  }

  reloadData() {
    this.router.navigate(['patient']);
  }

  editPatient(patient: Patient): void {
    window.localStorage.removeItem('editPatientId');
    window.localStorage.setItem('editPatientId', patient.id.toString());
    this.router.navigate(['edit-patient']);
  }


  patientDetail(id: number){
    this.router.navigate(['details', id]);
  }
}
