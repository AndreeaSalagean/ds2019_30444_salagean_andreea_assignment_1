import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreatePatientComponent } from './components/patient/create-patient/create-patient.component';
import { ListPatientComponent } from './components/patient/list-patient/list-patient.component';
import { EditPatientComponent } from './components/patient/edit-patient/edit-patient.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

// material
import {MatListModule} from '@angular/material/list';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CreateCaregiverComponent } from './components/caregiver/create-caregiver/create-caregiver.component';
import { ListCaregiverComponent } from './components/caregiver/list-caregiver/list-caregiver.component';
import { EditCaregiverComponent } from './components/caregiver/edit-caregiver/edit-caregiver.component';
import { DetailsPatientComponent } from './components/patient/details-patient/details-patient.component';
import { DetailsCaregiverComponent } from './components/caregiver/details-caregiver/details-caregiver.component';
import { CreateMedicationComponent } from './components/medication/create-medication/create-medication.component';
import { EditMedicationComponent } from './components/medication/edit-medication/edit-medication.component';
import { ListMedicationComponent } from './components/medication/list-medication/list-medication.component';
import { DetailsMedicationComponent } from './components/medication/details-medication/details-medication.component';
import { LoginComponent } from './components/login/login.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { HeaderComponent } from './components/header/header.component';
import {JwtInterceptor} from './helpers/jwt.interceptor';
import {ErrorInterceptor} from './helpers/errorInterceptor';
import { PopupListComponent } from './components/popup-list/popup-list.component';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonModule} from '@angular/material';
import { CaregiverPatientsComponent } from './components/caregiver/caregiver-patients/caregiver-patients.component';



@NgModule({
  declarations: [
    AppComponent,
    CreatePatientComponent,
    ListPatientComponent,
    EditPatientComponent,
    CreateCaregiverComponent,
    ListCaregiverComponent,
    EditCaregiverComponent,
    DetailsPatientComponent,
    DetailsCaregiverComponent,
    CreateMedicationComponent,
    EditMedicationComponent,
    ListMedicationComponent,
    DetailsMedicationComponent,
    LoginComponent,
    WelcomeComponent,
    HeaderComponent,
    PopupListComponent,
    CaregiverPatientsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    // services
    HttpClientModule,
    // material
    MatBottomSheetModule,
    MatListModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
