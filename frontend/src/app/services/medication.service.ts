import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Medication} from '../models/medication';

@Injectable({
  providedIn: 'root'
})
export class MedicationService {

  constructor(private http: HttpClient) { }
  baseUrl = 'http://localhost:8080/medication';

  getMedication(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

  deleteMedication(id: number): Observable<any> {
    return this.http.delete(this.baseUrl  + '/' + id);
  }
  getMedById(id: number): Observable<any> {
    return this.http.get<Medication>(this.baseUrl + '/' + id);
  }

  updateMedication(med: Medication): Observable<any> {
    return this.http.put(this.baseUrl + '/' +  med.id, med);
  }
  createMedication(med: Medication): Observable<any> {
    return this.http.post(this.baseUrl, med);
  }
}
