import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Patient} from '../models/patient';
import {Person} from '../models/person';
import {PersonPatient} from '../models/patperson';


@Injectable({
  providedIn: 'root'
})
export class PatientService {
  constructor(private http: HttpClient) { }
  baseUrl = 'http://localhost:8080/patient';

  getPatients(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
  deleteUser(id: number): Observable<any> {
    return this.http.delete(this.baseUrl  + '/' + id);
  }
  getUserById(id: number): Observable<any> {
    console.log(this.baseUrl + '/' + id);
    return this.http.get<Patient>(this.baseUrl + '/' + id);
  }

  updatePatient(patient: Patient): Observable<any> {
      return this.http.put(this.baseUrl + '/' +  patient.id, patient);
  }
  createPatient(patient: PersonPatient): Observable<any> {
    return this.http.post(this.baseUrl, patient);
  }

}
