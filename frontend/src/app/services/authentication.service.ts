import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Person} from '../models/person';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private currentUserSubject: BehaviorSubject<Person>;
  public currentUser: Observable<Person>;
  baseUrl = 'http://localhost:8080/person/authenticate';

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<Person>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): Person {
    return this.currentUserSubject.value;
  }
  login(username: string, password: string) {
    return this.http.post<any>(`${this.baseUrl}`, {  username, password})
      .pipe(map (user => {
        // login successful if there's a jwt token in the response
        if (user && user.role) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
        }
        return user;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
