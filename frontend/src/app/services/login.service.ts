import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  login(loginPayload): Observable<any> {
    return this.http.post('http://localhost:8080/' + 'token/generate-token', loginPayload);
  }
}
