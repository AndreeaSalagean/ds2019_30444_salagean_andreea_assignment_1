import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Person} from '../models/person';
import {Patient} from '../models/patient';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  baseUrl = 'http://localhost:8080/person';
  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Person[]>(`${this.baseUrl}`);
  }

  getById(id: number) {
    return this.http.get<Person>(this.baseUrl + '/' + id);
  }
}
