import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Patient} from '../models/patient';
import {PersonPatient} from '../models/patperson';
import {Caregiver} from '../models/caregiver';
import {PersonCaregiver} from '../models/carperson';

@Injectable({
  providedIn: 'root'
})
export class CaregiverService {

  constructor(private http: HttpClient) { }

  baseUrl = 'http://localhost:8080/caregiver';

  getCaregivers(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
  getPatients(id: number): Observable<any> {
    return this.http.get(this.baseUrl  + '/' + id + '/patients-list');
  }

  deleteCaregiver(id: number): Observable<any> {
    return this.http.delete(this.baseUrl  + '/' + id);
  }
  getUserById(id: number): Observable<any> {
    console.log(this.baseUrl + '/' + id);
    return this.http.get<Caregiver>(this.baseUrl + '/' + id);
  }

  updateCaregiver(caregiver: Caregiver): Observable<any> {
    return this.http.put(this.baseUrl + '/' +  caregiver.id, caregiver);
  }
  createCaregiver(caregiver: PersonCaregiver): Observable<any> {
    return this.http.post(this.baseUrl, caregiver);
  }
  addPatient(patient: Patient, id: number): Observable<any> {
    return this.http.post(this.baseUrl  + '/' + id + '/add-patient', patient);
  }
  deletePatient(id: number, idP: number): Observable<any> {
    return this.http.delete(this.baseUrl  + '/' + id + '/delete-patient' + '/' + idP);
  }
  getAvailablePat(id: number): Observable<any> {
    return this.http.get(this.baseUrl  + '/' + id + '/patients-list-available');
  }
}
